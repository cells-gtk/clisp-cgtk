(asdf:defsystem :cells-gtk
  :name "cells-gtk"
  :depends-on (:cells :gtk-ffi)
  :serial t
  :components
  ((:file "cells-gtk")   
   (:file "widgets")
   (:file "layout")
   (:file "display")
   (:file "buttons")
   (:file "entry")
   (:file "tree-view")
   (:file "menus")   
   (:file "dialogs")
   (:file "textview")
   (:file "addon")
   (:file "gtk-app")
))
