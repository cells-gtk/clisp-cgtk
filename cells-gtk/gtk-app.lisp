#|

 Cells Gtk

 Copyright (c) 2004 by Vasilis Margioulas <vasilism@sch.gr>

 You have the right to distribute and use this software as governed by 
 the terms of the Lisp Lesser GNU Public License (LLGPL):

    (http://opensource.franz.com/preamble.html)
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 Lisp Lesser GNU Public License for more details.
 
|#

(in-package :cgtk)

(defmodel gtk-app (window)
  ((splash-screen-image :accessor splash-screen-image :initarg :splash-screen-image :initform nil)
   (tooltips :accessor tooltips :initform (make-be 'tooltips))
   (tooltips-enable :accessor tooltips-enable :initarg :tooltips-enable :initform (c-in t))
   (tooltips-delay :accessor tooltips-delay :initarg :tooltips-delay :initform (c-in nil)))
  (:default-initargs
      :on-delete-event (lambda (widget event data)
			 (declare (ignore widget event data))
			 (gtk-main-quit))))

(def-c-output tooltips-enable ((self gtk-app))
  (if new-value
      (gtk-tooltips-enable (id (tooltips self)))
      (gtk-tooltips-disable (id (tooltips self)))))

(def-c-output tooltips-delay ((self gtk-app))
  (when new-value
      (gtk-tooltips-set-delay (id (tooltips self)) new-value)))
      
(defmodel splash-screen (window)
  ((image-path :accessor image-path :initarg :image-path :initform nil))
  (:default-initargs
      :decorated nil
      :position :center
      :kids (c? (when (image-path self)
		  (list
		   (mk-image :filename (image-path self)))))))

(defvar *gtk-initialized* nil)

(defun start-app (app-name &key debug)
  (let ((*gtk-debug* debug))
    (when (not *gtk-initialized*)
      (when *gtk-debug*
	(trc "GTK INITIALIZATION") (force-output))
      (g-thread-init nil)
      (gdk-threads-init)
      (assert (gtk-init-check nil nil))
      (setf *gtk-initialized* t))

    (with-gdk-threads
	(let ((app (make-instance app-name :visible (c-in nil)))
	      (splash))
	  (when (splash-screen-image app)
	    (setf splash (make-instance 'splash-screen :image-path (splash-screen-image app)
					:visible (c-in nil)))
	    (gtk-window-set-auto-startup-notification nil)
	    (to-be splash)
	    (setf (visible splash) t)
	    (loop while (gtk-events-pending) do
		 (gtk-main-iteration)))
	  
	  (to-be app)
	  
	  (when splash
	    (not-to-be splash)
	    (gtk-window-set-auto-startup-notification t))
	  
	  (setf (visible app) t)
	  
	  (when *gtk-debug*
	    (trc "STARTING GTK-MAIN") (force-output))
	  (gtk-main)))))

(export '(gtk-app title icon tooltips tooltips-enable tooltips-delay
	  start-app))