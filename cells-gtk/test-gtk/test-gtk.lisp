(defpackage :test-gtk
  (:use :common-lisp :utils-kt :cells :cells-gtk))

(in-package :test-gtk)

(defmodel test-gtk (gtk-app)
  ()
  (:default-initargs
    :title "GTK Testing"
    :icon "test-images/small.png"
    :position :center
    :splash-screen-image "test-images/splash.png"
    :width 550 :height 550
    :kids (list
	   (mk-notebook 
	    :tab-labels '("Buttons" "Entry" "Display" "Layout" "Menus"
			  "Tree view" "Text view" "Dialogs" "Addons")
	    :kids (loop for test-name in '(test-buttons test-entry test-display test-layout test-menus
					   test-tree-view test-textview test-dialogs test-addon)
			collect (make-instance test-name))))))

(defun test-gtk-app ()
  (start-app 'test-gtk)
  #+clisp (ext:exit))

;(ext:saveinitmem "test-gtk.mem" :init-function 'test-gtk::test-gtk-app)
