(in-package :test-gtk)

(defmodel test-textview (vbox)
  ((buffer :accessor buffer :initarg :buffer 
	   :initform (mk-text-buffer 
		      :text (format nil "~{~a~%~}" (loop for i below 100 collect (format nil "Text view Line ~a" i))))))
  (:default-initargs
      :kids (list
	     (mk-scrolled-window 
	      :kids (list
		     (mk-text-view 	      
		      :buffer (c? (buffer (upper self test-textview)))))))))
