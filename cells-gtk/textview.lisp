#|

 Cells Gtk

 Copyright (c) 2004 by Vasilis Margioulas <vasilism@sch.gr>

 You have the right to distribute and use this software as governed by 
 the terms of the Lisp Lesser GNU Public License (LLGPL):

    (http://opensource.franz.com/preamble.html)
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 Lisp Lesser GNU Public License for more details.
 
|#

(in-package :cgtk)

(def-object text-buffer ()
  ((text :accessor text :initarg :text :initform nil))
  ()
  ()
  :new-args (c? (list nil)))

(def-c-output text ((self text-buffer))
  (with-gtk-string (txt (or new-value ""))
    (gtk-text-buffer-set-text (id self) 
			      txt
			      -1)))

(def-widget text-view ()
  ((buffer :accessor buffer :initarg :buffer :initform (mk-text-buffer)))
  ()
  ()
  :kids (c? (when (buffer self) (list (buffer self))))
  :expand t
  :fill t)

(def-c-output buffer ((self text-view))
  (when new-value
    (gtk-text-view-set-buffer (id self) (id (buffer self)))))
