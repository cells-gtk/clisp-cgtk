#|

 Cells Gtk

 Copyright (c) 2004 by Vasilis Margioulas <vasilism@sch.gr>

 You have the right to distribute and use this software as governed by 
 the terms of the Lisp Lesser GNU Public License (LLGPL):

    (http://opensource.franz.com/preamble.html)
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 Lisp Lesser GNU Public License for more details.
 
|#

(in-package :cgtk)

(def-object list-store ()
  ((item-types :accessor item-types :initarg :item-types :initform nil))
  ()
  ()
  :new-args (c? (list (item-types self))))

(def-object tree-store ()
  ((item-types :accessor item-types :initarg :item-types :initform nil))
  ()
  ()
  :new-args (c? (list (item-types self))))

(defun fail (&rest args) nil)

(def-widget tree-view ()
  ((columns-def :accessor columns-def :initarg :columns :initform nil)
   (column-types :accessor column-types :initform (c? (mapcar #'first (columns-def self))))
   (column-inits :accessor  column-inits :initform (c? (mapcar #'second (columns-def self))))
   (column-render :accessor column-render 
		  :initform (c? (loop for col-def in (columns-def self)
				      for pos from 0 append
				      (when (third col-def)
					(list pos (third col-def))))))
   (columns :accessor columns
	    :initform (c? (mapcar #'(lambda (col-init) 
				      (apply #'make-be 'tree-view-column col-init))
				  (column-inits self))))
   (select-if :unchanged-if #'fail
	      :accessor select-if :initarg :select-if :initform (c-in nil))
   (items :accessor items :initarg :items :initform nil)
   (items-factory :accessor items-factory :initarg :items-factory :initform #'identity)
   (selection-mode :accessor selection-mode :initarg :selection-mode :initform :single)
   (on-select :accessor on-select :initarg :on-select :initform nil)
   (tree-model :accessor tree-model :initarg :tree-model :initform nil))
  ()
  ()
  :on-select (callback (widget event data)
	       (setf (md-value self) (get-selection self))))

(def-c-output tree-model ((self tree-view))
  (when new-value
    (gtk-tree-view-set-model (id self) (id (to-be new-value)))))

(defun item-from-path (tree path)
  (let ((start-node (nth (first path) tree)))
    (loop with target-node = start-node
       for node-index in (rest path)       
       for node = (when node-index 
		    (nth node-index (kids target-node))) do
	 (when node
	   (setf target-node node))
	 finally (return target-node))))

(defmethod get-selection ((self tree-view))
  (let ((selection (gtk-tree-view-get-selection (id self))))
    (let (sel)
      (gtk-tree-selection-selected-foreach selection
	#'(lambda (model path iter data)	    
	      (push (item-from-path 
		     (items self) 
		     (read-from-string 
		      (gtk-tree-model-get-cell model iter (length (column-types self)) :string)))
		    sel))
	nil)
      (if (equal (gtk-tree-selection-get-mode selection) 3) ;;multiple
	  sel
	  (first sel)))))

(def-c-output selection-mode ((self tree-view))
  (when new-value
    (let ((sel (gtk-tree-view-get-selection (id self))))
      (gtk-tree-selection-set-mode sel 
	 (ecase (selection-mode self)
	   (:none 0)
	   (:single 1)
	   (:browse 2)
	   (:multiple 3))))))

(def-c-output on-select ((self tree-view))
  (when new-value    
    (let ((sel (gtk-tree-view-get-selection (id self))))
      (gtk-signal-connect sel "changed" (on-select self)))))

(defmodel listbox (tree-view)
  ()
  (:default-initargs 
      :tree-model (c? (make-instance 'list-store
				:item-types (append (column-types self) (list :string))))))

(defun mk-listbox (&rest inits)
  (apply 'make-instance 'listbox inits))

(def-c-output select-if ((self listbox))
  (when new-value
    (setf (md-value self) (remove-if-not new-value (items self)))))

(def-c-output items ((self listbox))
  (when old-value
    (gtk-list-store-clear (id (tree-model self))))
  (when new-value
    (gtk-list-store-set-items 
     (id (tree-model self)) 
     (append (column-types self) (list :string))
     (loop for item in new-value
	  for index from 0 collect
	  (append
	   (funcall (items-factory self) item)
	   (list (format nil "(~d)" index)))))))

(defmodel treebox (tree-view)
  ()
  (:default-initargs 
      :tree-model (c? (make-instance 'tree-store
				:item-types (append (column-types self) (list :string))))))

(defun mk-treebox (&rest inits)
  (apply 'make-instance 'treebox inits))

(def-c-output select-if ((self treebox))
  (when new-value
    (setf (md-value self) (mapcan (lambda (item) (fm-collect-if item new-value)) 
				  (items self)))))

(def-c-output items ((self treebox))
  (when old-value
    (gtk-tree-store-clear (id (tree-model self))))
  (when new-value
    (loop for sub-tree in new-value
       for index from 0 do
	 (gtk-tree-store-set-kids (id (tree-model self)) sub-tree nil index
				  (append (column-types self) (list :string)) 
				  (items-factory self)))))

(def-c-output columns ((self tree-view))
  (when new-value
    (loop for col in new-value
	  for pos from 0	  
	  for renderer = (case (nth pos (column-types self))
			     (:boolean (gtk-cell-renderer-toggle-new))
			     (:icon (gtk-cell-renderer-pixbuf-new))
			     (t (gtk-cell-renderer-text-new))) do
	  (gtk-tree-view-column-pack-start (id col) renderer t)
	  (gtk-tree-view-column-set-cell-data-func (id col) renderer
						   (gtk-tree-view-render-cell pos 
									      (nth pos (column-types self))
									      (getf (column-render self) pos))
						   nil
						   nil)
	 (gtk-tree-view-column-set-sort-column-id (id col) pos)
	 (gtk-tree-view-insert-column (id self) (id col) pos))))
    
(def-object tree-view-column ()
  ((title :accessor title :initarg :title :initform nil)
   (visible :accessor visible :initarg :visible :initform t))
  (spacing resizable fixed-width min-width max-width expand clickable
   sort-column-id sort-indicator reorderable)
  ()
  :resizable t
  :expand t
  :reorderable t)

(def-c-output visible ((self tree-view-column))
  (gtk-tree-view-column-set-visible (id self) new-value))

(def-c-output title ((self tree-view-column))
  (when new-value
    (with-gtk-string (str new-value)
      (gtk-tree-view-column-set-title (id self) str))))

(defmacro def-columns (&rest args)
  `(list ,@(loop for (type inits renderer) in args collect
		 `(list ,type ',inits ,renderer))))

(export '(mk-listbox mk-treebox def-columns))