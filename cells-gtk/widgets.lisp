#|

 Cells Gtk

 Copyright (c) 2004 by Vasilis Margioulas <vasilism@sch.gr>

 You have the right to distribute and use this software as governed by 
 the terms of the Lisp Lesser GNU Public License (LLGPL):

    (http://opensource.franz.com/preamble.html)
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 Lisp Lesser GNU Public License for more details.
 
|#

(in-package :cgtk)

(defmodel gtk-object (family)
  ((def-gtk-class-name :accessor def-gtk-class-name :initarg :def-gtk-class-name :initform nil)
   (new-function-name :accessor new-function-name :initarg :new-function-name 
		      :initform (c? (intern (format nil "GTK-~a-NEW~a"
						    (def-gtk-class-name self)
						    (or (new-tail self) ""))
					    :gtk-ffi)))
   (new-args :accessor new-args :initarg :new-args :initform nil)
   (new-tail :accessor new-tail :initarg :new-tail :initform nil)
   (id :initarg :id :accessor id 
       :initform (c? (without-c-dependency 
		      (when *gtk-debug* 
			(trc "NEW" (new-function-name self) (new-args self)) (force-output))
		      (apply (symbol-function (new-function-name self)) (new-args self))))))
  (:default-initargs
      :md-name (c-in nil)
      :md-value (c-in nil)))

(defmethod configure ((self gtk-object) gtk-function value)
  (apply gtk-function (id self) (if (consp value) value (list value))))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun gtk-function-name (class option)
    (let ((slot-access (if (atom option)
			   (list 'set option)
			   (append (and (second option) (list (second option)))
				   (list (first option))))))
      (intern (format nil "GTK-~a~{-~a~}" class slot-access) :gtk-ffi))))

;;; --- widget --------------------

(eval-when (:compile-toplevel :load-toplevel :execute)

  (defmacro def-object (&rest args)
    `(def-gtk gtk-object ,@args))
  (defmacro def-widget (&rest args)
    `(def-gtk widget ,@args))  
  (defmacro def-gtk (gtk-superclass class
		     superclasses
		     (&rest std-slots)
		     (&rest gtk-slots) (&rest gtk-signals) &rest defclass-options)
  (multiple-value-bind (slots outputs)
          (loop for gtk-option-def in gtk-slots
              for slot-name = (if (atom gtk-option-def)
				  gtk-option-def (car gtk-option-def))
              collecting `(,slot-name :initform (c-in nil)
				      :initarg ,(intern (string slot-name) :keyword)
				      :accessor ,slot-name)
              into slot-defs
              collecting `(def-c-output ,slot-name ((self ,class))
                            (when (or new-value old-value)
			      (when *gtk-debug* (TRC ,(format nil "~a-~a" class slot-name) new-value) (force-output))
                              (configure self #',(gtk-function-name class gtk-option-def)
					 new-value))
			   (call-next-method))

              into outputs
              finally (return (values slot-defs outputs)))
    (multiple-value-bind (signals-slots signals-outputs)
	(loop for signal-slot in gtk-signals
              for slot-name = (intern (format nil "ON-~a" signal-slot))
              collecting `(,slot-name :initform nil
				      :initarg ,(intern (string slot-name) :keyword)
				      :accessor ,slot-name)
              into signals-slots-defs
              collecting `(def-c-output ,slot-name ((self ,class))
                            (when new-value
			      (gtk-signal-connect (id self) ,(string-downcase (string signal-slot)) new-value))
			   (call-next-method))
              into signals-outputs-defs
              finally (return (values signals-slots-defs signals-outputs-defs)))
        `(progn
	  (defmodel ,class ,(or superclasses (list gtk-superclass))
	    (,@(append std-slots slots signals-slots))
	    (:default-initargs
		:def-gtk-class-name ',class
		,@defclass-options))
	  (export ',class)
	  (export ',(mapcar #'first (append std-slots slots signals-slots)))
	  
	  (defun ,(intern (format nil "MK-~a" class)) (&rest inits)
	    (when *gtk-debug* (trc "MAKE-INSTANCE" ',class) (force-output))
	    (apply 'make-instance ',class inits))
	  (export ',(intern (format nil "MK-~a" class)))
	  ,@outputs
	  ,@signals-outputs)))))

(defmacro callback ((widg event data) &body body)
  `(c? (without-c-dependency #'(lambda (,widg ,event ,data) 
				 (declare (ignorable ,widg ,event ,data))
				 ,@body t))))
(defmacro callback-if (condition (widg event data) &body body)
  `(c? (and ,condition
	(without-c-dependency #'(lambda (,widg ,event ,data) 
				  (declare (ignorable ,widg ,event ,data))
				  ,@body t)))))

(defun timeout-add (milliseconds function)
  (g-timeout-add milliseconds 
		 #'(lambda (x)
		     (declare (ignore x))
		     (with-gdk-threads
			 (funcall function)))
		 nil))

(def-object widget ()
  ((tooltip :accessor tooltip :initarg :tooltip :initform (c-in nil))
   (popup :accessor popup :initarg :popup :initform (c-in nil))
   (visible :accessor visible :initarg :visible :initform (c-in t))
   (sensitive :accessor sensitive :initarg :sensitive :initform (c-in t))
   (expand :accessor expand? :initarg :expand :initform nil)
   (x-expand :accessor x-expand :initarg :x-expand :initform (c? (expand? self)))
   (y-expand :accessor y-expand :initarg :y-expand :initform (c? (expand? self)))
   (fill :accessor fill? :initarg :fill :initform nil)
   (x-fill :accessor x-fill :initarg :x-fill :initform (c? (fill? self)))
   (y-fill :accessor y-fill :initarg :y-fill :initform (c? (fill? self)))
   (padding :accessor padding? :initarg :padding :initform 2)
   (x-pad :accessor x-pad :initarg :x-pad :initform (c? (padding? self)))
   (y-pad :accessor y-pad :initarg :y-pad :initform (c? (padding? self)))
   (width :accessor width :initarg :width :initform nil)
   (height :accessor height :initarg :height :initform nil))
  ()
  (focus show hide delete-event destroy-event))

(defmethod focus ((self widget))
  (gtk-widget-grab-focus (id self)))

(def-c-output width ((self widget))
  (when new-value
    (gtk-widget-set-size-request (id self)
				 new-value
				 (or (height self) -1))))

(def-c-output height ((self widget))
  (when new-value
    (gtk-widget-set-size-request (id self)
				 (or (width self) -1)
				 new-value)))

(def-c-output sensitive ((self widget))
  (gtk-widget-set-sensitive (id self) new-value))
  
(def-c-output popup ((self widget))
  (when old-value
    (not-to-be old-value))
  (when new-value
    (gtk-widget-set-popup (id self) (id (to-be new-value)))))
    
(def-c-output visible ((self widget))
  (when *gtk-debug*
	(trc "VISIBLE" (md-name self) new-value)  (force-output))
  (if new-value
    (gtk-widget-show (id self))
    (gtk-widget-hide (id self)))) 

(def-c-output tooltip ((self widget))
  (when new-value
    (with-gtk-string (str new-value)
      (gtk-tooltips-set-tip (id (tooltips (upper self gtk-app)))
				     (id self)
				     str
				     ""))))

(defmethod not-to-be :after ((self widget))
  (when *gtk-debug* (trc "WIDGET DESTROY" (md-name self)) (force-output))
  (gtk-widget-destroy (id self)))

(defun assert-bin (container)
  (assert (null (rest (kids container))) 
	  ()
	  "~a is a bin container, must have only one kid" container))

(def-widget window ()
  ((wintype :accessor wintype :initarg wintype :initform 0)
   (title :accessor title :initarg :title :initform (c? (string (class-name (class-of self)))))
   (icon :initarg :icon :accessor icon :initform nil)
   (decorated :accessor decorated :initarg :decorated :initform (c-in t))
   (position :accessor set-position :initarg :position :initform (c-in nil))
   (accel-group :accessor accel-group :initform (gtk-accel-group-new)))
  (default-size resizable
   (maximize) (unmaximize) (iconify) (deiconify) (fullscreen) (unfullscreen))
  ()
;  :md-name :main-window
  :new-args (c? (list (wintype self))))

(def-c-output width ((self window))
  (when new-value
    (gtk-window-set-default-size (id self)
				 new-value
				 (or (height self) -1))))

(def-c-output height ((self window))
  (when new-value
    (gtk-window-set-default-size (id self)
				 (or (width self) -1)
				 new-value)))

(def-c-output accel-group ((self window))
  (when new-value
    (gtk-window-add-accel-group (id self) new-value)))

(def-c-output title ((self window))
  (when new-value
    (with-gtk-string (str new-value)
      (gtk-window-set-title (id self) str))))

(def-c-output icon ((self window))
  (when new-value
    (gtk-window-set-icon-from-file (id self) new-value nil)))

(def-c-output decorated ((self window))
  (gtk-window-set-decorated (id self) new-value))

(def-c-output position ((self window))
  (when new-value
    (gtk-window-set-position (id self)
      (ecase new-value
	(:none 0)
	(:center 1)
	(:mouse 2)
	(:center-always 3)
	(:center-on-parent 4)))))

(def-c-output .kids ((self window))
  (assert-bin self)
  (dolist (kid new-value)
    (when *gtk-debug* (trc "WINDOW ADD KID" (md-name self) (md-name kid)) (force-output))
    (gtk-container-add (id self) (id kid)))
  (call-next-method))

(def-widget event-box ()
  ((visible-window :accessor visible-window :initarg :visible-window :initform nil))
  (above-child)
  ()
  :above-child t)

(def-c-output visible-window ((self event-box))
  (gtk-event-box-set-visible-window (id self) new-value))
  
(def-c-output .kids ((self event-box))
  (assert-bin self)
  (when new-value
    (gtk-container-add (id self) (id (first new-value))))
  (call-next-method))


(export '(callback callback-if timeout-add focus))
