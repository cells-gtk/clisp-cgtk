(defparameter *utils-kt-path* "../utils-kt/")
(defparameter *cells-path* "../cells/")

#-asdf (load (make-pathname :name "asdf" :type "lisp"))

(pushnew *utils-kt-path* asdf:*central-registry*)
(pushnew *cells-path* asdf:*central-registry*)
(pushnew "./gtk-ffi/" asdf:*central-registry*)
(pushnew "./cells-gtk/" asdf:*central-registry*)
(pushnew "./cells-gtk/test-gtk/" asdf:*central-registry*)

(asdf:operate 'asdf:load-op :cells-gtk :force nil)
(asdf:operate 'asdf:load-op :test-gtk :force nil)

(defun gtk-demo ()
   (cells-gtk:start-app 'test-gtk::test-gtk))